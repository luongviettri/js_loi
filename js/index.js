import { Products } from "./model.js";
import { batLoading, tatLoading, renderLayout, searchSelected, getProducts, handleAddItem, displayCart, handleDecrease, handleIncrease, thanhToan, removeCart, saveOnLocalStorage } from "./controller.js"
window.searchSelected = searchSelected;
window.handleAddItem = handleAddItem;
window.displayCart = displayCart;
window.handleDecrease = handleDecrease;
window.handleIncrease = handleIncrease;
window.thanhToan = thanhToan;
window.removeCart = removeCart;
window.saveOnLocalStorage = saveOnLocalStorage;

// ! URL API
/**
 *  hàm hiện danh sách sản phẩm
 */
const displayProducts = async () => {
    // ! lấy sản phẩm về
    let products = await getProducts();
    console.log('products: ', products);
    // ! render ra giao diện
    renderLayout(products);
    // ! lưu sản phẩm lên localStorage để tìm cho nhanh
    saveOnLocalStorage(products);
}
// ! Gọi hàm hiện danh sách sản phẩm
displayProducts();
// ! hiển thị giỏ hàng
displayCart();



