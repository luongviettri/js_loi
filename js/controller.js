
const BASE_URL = "https://62ebce02705264f263e0cad5.mockapi.io/Products"
let gioHang = [];
let sanPham = [];
// ! tạo key local storage
const GIOHANG = "GIOHANG";
const SANPHAM = "SANPHAM";
// ! lấy dữ liệu từ local storage về
let gioHangJSON = localStorage.getItem(GIOHANG);
if (gioHangJSON) {
    gioHang = JSON.parse(gioHangJSON);
}
// ! gắn hàm render carts
let btnRender = document.getElementById("btn-render");
console.log('btnRender: ', btnRender);
btnRender.addEventListener('click', () => {
    renderCarts(gioHang);
})

export function batLoading() {
    document.getElementById('loading').style.display = "flex";
}
export function tatLoading() {
    document.getElementById('loading').style.display = "none";
}
/**
 * ! hàm render giao diện sản phẩm
 * @param {*} products 
 */
export const renderLayout = (products) => {

    // !DOM đến div chứa
    let displayEl = document.getElementById('hienThiProducts');
    // ! lặp và render hoy
    let rows = '';
    products.forEach((product) => {
        let row =
            `
        <div class="card" style="width: 18rem;">

        <img class="card-img-top" src="${product.img}"
            alt="Card image cap">

        <div class="card-body">
            <h5 class="card-title">${product.type.toUpperCase()}</h5>
            <h6 class="card-title">${product.name}</h6>
            <p class="card-text text-danger font-weight-bold">$${product.price}</p>
            <a
            
            href="#" 
            class="btn btn-primary h-25 btn-add"
            onclick="handleAddItem(${product.id})"
            >Thêm vào giỏ hàng!!!</a>
        </div>
    </div>
        `
        rows += row;
    })
    displayEl.innerHTML = rows;
}

export const saveOnLocalStorage = (sanPham) => {
    // ! lưu lên local Storage
    var sanPhamJSON = JSON.stringify(sanPham);
    localStorage.setItem(SANPHAM, sanPhamJSON);
}

/**
* !hàm xử lý select search
*/
export const searchSelected = async () => {
    //! lấy thông tin search
    let typeSearch = getInforSearch();
    // ! gửi lên API tìm tên này
    let found = searchingProducts(typeSearch);
    // !render lại giao diện theo sản phẩm tìm được
    renderLayout(found);
}
const getInforSearch = () => {
    let needToDisplay = document.getElementById("txt-search");
    let valueNeedToDisplay = needToDisplay.options[needToDisplay.selectedIndex].value;
    return valueNeedToDisplay;
}
/**
 * !hàm dùng để tìm sản phẩm
 */
const searchingProducts = (typeSearch) => {
    // ! tìm tất cả sản phẩm
    let allProducts = getItemFromJson();
    // ! nếu typeSearch là all
    if (typeSearch == "All") {
        let lookFor = allProducts;
        return lookFor;
    }
    // ! filter ra mảng mới theo loại cần tìm
    let lookFor = allProducts.filter((product) => {
        return product.type.toLowerCase().includes(typeSearch.toLowerCase());
    })
    return lookFor;
}
/**
 *!  hàm getProducts để lấy sản phẩm về
 * @returns Array
 */
export const getProducts = async () => {
    batLoading();
    let allProducts = await axios({
        url: BASE_URL,
        method: "GET"
    }).then((res) => {
        tatLoading();
        return res.data
    }).catch((err) => {
        tatLoading();
        console.log('err: ', err);
    })
    return allProducts;
}
const getItemFromJson = () => {
    // ! lấy dữ liệu từ local storage về
    let sanPhamJSON = localStorage.getItem(SANPHAM);
    if (sanPhamJSON) {
        sanPham = JSON.parse(sanPhamJSON);
    }
    return sanPham;
}
/**
 * !hàm xử lý add sản phẩm vào giỏ hàng
 */
export const handleAddItem = (idItem) => {
    // !: tìm đối tượng sản phẩm trên local Storage
    let foundObject = findObjects(idItem);
    // !: vô mảng cart xem có đối tượng này chưa
    let isFound = checkCarts(idItem);
    if (!isFound) {
        // !: nếu chưa có thì...
        handleNotFound(foundObject);
    } else {
        // !: nếu có rồi thì...
        handleFound(idItem);
    }
    displayCart();
}
const handleFound = (idItem) => {
    // ! tìm đến đối tượng đó và tăng trường số lượng lên 1
    gioHang.forEach((cart) => {
        if (cart.id == idItem) {
            cart.quantity++;
        }
    })
    // ! lưu lên local Storage
    var gioHangJSON = JSON.stringify(gioHang);
    localStorage.setItem(GIOHANG, gioHangJSON);
}
const handleNotFound = (foundObject) => {
    // !: gán thành đối tượng mới có thêm trường 
    let newCart = { ...foundObject, quantity: 1 };
    // ! push vào mảng
    gioHang.push(newCart);
    // ! lưu lên local Storage
    var gioHangJSON = JSON.stringify(gioHang);
    localStorage.setItem(GIOHANG, gioHangJSON);

}
const findObjects = (idItem) => {
    // !lấy sản phẩm từ JSON
    let sanPham = getItemFromJson();
    // ! tìm sp
    let sanPhamCanTim = sanPham.find((item) => {
        return item.id == idItem;
    }
    )
    return sanPhamCanTim;
}
const checkCarts = (idCanTim) => {
    let isFound = false;
    // ! duyệt mảng gioHang tìm xem có ID ko

    gioHang.forEach((cart) => {
        if (cart.id == idCanTim) {
            isFound = true;
        }
    })
    return isFound;
}
// ! render giỏ hàng
export const displayCart = () => {
    let carts = document.getElementById('number-carts');
    let totalCarts = getTotalCarts();
    carts.innerHTML = totalCarts;
}
const getTotalCarts = () => {
    let num = 0;
    gioHang.forEach((cart) => {
        num += cart.quantity;
    })
    return num;
}
/**
 * ! hàm dùng để render giao diện hàng đã chọn
 * @param {} carts 
 */
const renderCarts = (carts) => {
    // !DOM đến div chứa
    let cartsEl = document.getElementById('carts');
    let totalProduct = document.getElementById('totalProduct');
    let btnThanhToan = document.getElementById('btn-thanh-toan');
    btnThanhToan.classList.add('d-flex');
    btnThanhToan.classList.remove('d-none');
    // ! lặp và render hoy
    let rows = '';
    let total;
    let allTotalProducts = 0;
    carts.forEach((cart) => {
        total = cart.price * cart.quantity;
        allTotalProducts += total;
        let row =
            `
        
        <div class="cardZZZZ text-light row my-2 border border-success" style="width: 100%; height:10rem">
            <img class="ml-5 col-2"  src="${cart.img}"
            alt="Card image cap">   
            <div class="card-body align-items-center row ml-5">
                <h5 class="card-title m-0 col-3">${cart.name}</h5>
                <div class='col-3' >
                    <span onclick="handleDecrease(${cart.id})" class="text-danger  d-inline-block ">
                    <i class="fa fa-angle-left"></i>    
                    </span>
                    <span class="text-primary d-inline-block ">Số lượng: ${cart.quantity} </span> 
                    <span onclick="handleIncrease(${cart.id})" class="text-danger d-inline-block">
                    <i class="fa fa-angle-right"></i>
                    </span>
                </div>
                <div class="text-center col-3" >
                    <p class="m-0"> Tổng tiền: ${total}  </p>

                </div>
                <div class = " col-3">
                <button onclick="removeCart(${cart.id})" class= "btn btn-danger" >Remove </button>
                </div>
            </div>

        </div>
        `
        rows += row;
    })
    cartsEl.innerHTML = rows;
    totalProduct.innerHTML = `
    <p>Tổng tiền là </p>
    <p>${allTotalProducts} <p>
    `

}
export const handleDecrease = (id) => {
    gioHang.forEach((cart) => {
        if (cart.id == id) {
            // ! nếu số lượng sản phẩm là 1 thì ko cho giảm nữa
            if (cart.quantity <= 1) {
                return;
            }
            cart.quantity--;
        }
    })
    // ! lưu lên local Storage
    var gioHangJSON = JSON.stringify(gioHang);
    localStorage.setItem(GIOHANG, gioHangJSON);
    renderCarts(gioHang);
    displayCart();
}
export const handleIncrease = (id) => {
    gioHang.forEach((cart) => {
        if (cart.id == id) {
            cart.quantity++;
        }
    })
    renderCarts(gioHang);
    // ! lưu lên local Storage
    var gioHangJSON = JSON.stringify(gioHang);
    localStorage.setItem(GIOHANG, gioHangJSON);
    displayCart();
}
export const thanhToan = () => {
    // clear giao diện giỏ hàng
    clearRenderCarts();
    // set lại mảng giỏ hàng là rỗng
    setEmptyCarts();
    // render lại
    displayCart();
}
const clearRenderCarts = () => {
    // !DOM đến div chứa
    let cartsEl = document.getElementById('carts');
    let totalProduct = document.getElementById('totalProduct');
    let btnThanhToan = document.getElementById('btn-thanh-toan');

    cartsEl.innerHTML = '';
    totalProduct.innerHTML = '';
    btnThanhToan.classList.add('d-none');
    btnThanhToan.classList.remove('d-flex');

}
const setEmptyCarts = () => {
    // ! lưu lên local Storage
    gioHang = [];
    var gioHangJSON = JSON.stringify(gioHang);
    localStorage.setItem(GIOHANG, gioHangJSON);

}
// ! xóa sản phẩm khỏi giỏ hàng
export const removeCart = (CartID) => {
    // ! tìm đến mảng giỏ hàng
    // ! xóa đối tượng đó ra khỏi mảng giỏ hàng
    let gioHangMoi = gioHang.filter((cart) => {
        return cart.id != CartID;
    })
    gioHang = gioHangMoi;
    //! render lại giao diện giỏ hàng
    renderCarts(gioHang);
    //! render lại giỏ hàng
    displayCart();
    // ! lưu trên local Storage
    var gioHangJSON = JSON.stringify(gioHang);
    localStorage.setItem(GIOHANG, gioHangJSON);
}




